<?php

namespace App\Console\Commands;
use Carbon\Carbon;
use Illuminate\Console\Command;

/**
 * Created by PhpStorm.
 * User: mabeh
 * Date: 7.08.2018
 * Time: 14:25
 */
class Pysr extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "pysr:input   {argument} ";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "pysr calculate Commission Fees ";

    protected $currencies = [
        'USD' =>1.1497,
        'JPY' =>129.179387,
        'EUR' =>1,
    ];
    protected $comission_fees = [
        'cash_in'=>[
            'natural'=>[
                'default'=>0.03, //  commission fee
                'max'=>5, // maximum fee 5 EUR
                'min'=> 0.03, // minimum fee EUR
                'currency'=>'EUR' // fee currency
            ],
            'legal'=>[
                'default'=>0.03, //  commission fee
                'max'=>5, // maximum fee 5 EUR
                'min'=> 0.50, // minimum fee EUR
                'currency'=>'EUR' // fee currency
            ]

        ],
        'cash_out'=>[
            'natural'=>[
                'default'=>0.3,
                'min'=> 0.3,
                'currency'=>'EUR',
            ],
            'legal'=>[
                'default'=>0.3,
                'min'=> 0.50,
                'currency'=>'EUR',
            ]

        ],
        'fee_limit'=>1000,
        'fee_limit_currency'=>'EUR'
    ];


    protected function setFee($fee){

        return number_format($fee,2,'.','');

    }
    /**
     *Currency for Commission Fee
     * Commission fee is always calculated in the currency of particular operation (for example, if you cash out USD, commission fee is also in USD).
     * Now i found currency change all to EUR
     */
    protected  function setAmount($amount,$currency){

        return floor($amount / $this->currencies[$currency]);
    }

    protected function getCommissionFee($operation_type,$user_type,$value){
        return $this->comission_fees[$operation_type][$user_type][$value];
    }

    protected function setCommissionFee($operation_type,$user_type,$key,$value){
        return $this->comission_fees[$operation_type][$user_type][$key] = $value;
    }


    protected  function calculateFee($data){


        $amount = $this->setAmount($data['amount'],$data['currency']);
        $default_fee = $this->getCommissionFee($data['operation_type'],$data['user_type'],'default');
        $default_min =  $this->getCommissionFee($data['operation_type'],$data['user_type'],'min');


        $fee = $amount * $default_fee / 100;

        if($fee < $default_min){
            $fee = $default_min;
        }

        if($data['operation_type'] == 'cash_in'){
            $default_max = $this->getCommissionFee($data['operation_type'],$data['user_type'],'max');
            if($fee > $default_max){
                $fee = $default_max;
            }
        }

        return $fee;


    }
    /**

     * This function is do -This discount is applied only for first 3 cash out operations per week for each user - for forth and other operations commission is calculated by default rules (0.3%) - rule about 1000 EUR is applied only for first three cash out operations.
     */

    protected  function freeFeeWeekly($data,$limit){



        $amount = $this->setAmount($data['amount'],$data['currency']);

        if($limit <= $this->comission_fees['fee_limit']){
            $fee = 0;
        } else {

            $default_fee = $this->getCommissionFee($data['operation_type'],$data['user_type'],'default');
            $amount = $amount - $this->comission_fees['fee_limit'];
            $fee = $amount * $default_fee / 100;
        }

        if($data['operation_type'] == 'cash_in'){
            $default_max = $this->getCommissionFee($data['operation_type'],$data['user_type'],'max');
            if($fee > $default_max){
                $fee = $default_max;
            }
        }


        return $fee;



    }

    /**
     *This  function get weekly operation types make groups for calculate commission fees
     */
    protected function calculateCommissionWeekly($listWeekly){

        $fee = [];
        foreach($listWeekly as $week=>$data){
            $weekProcessCount = 1;
            $weeklyAmount[$week] = 0;
                foreach ($data as $d){

                    $weeklyAmount[$week]+= $this->setAmount($d['amount'],$d['currency']);
                    if($weekProcessCount < 3 ){ // discount is applied only  natural users  and first 3 cash_out;

                        if($d['user_type'] == 'natural' && $d['operation_type'] == 'cash_out'){
                            $fee[] = $this->freeFeeWeekly($d,$weeklyAmount[$week]); //1000.00 EUR per week (from monday to sunday) is free of charge. If total cash out amount is exceeded - commission is calculated only from exceeded amount (that is, for 1000.00 EUR there is still no commission fee).
                        } else {
                            $fee[] = $this->calculateFee($d);
                        }


                    }  else {

                        $fee[] = $this->calculateFee($d);
                    }


                    $weekProcessCount++;
                }


        }

        return $fee;

    }

    protected function getWeeklyOperationTypes($operations,$type){

        $weekly = [];

        if(!isset($operations[$type])){

            $operations[$type] = [];
        }


        foreach($operations[$type] as $operation){
            $dt = Carbon::parse($operation['date']);
            $weekNumberInMonth = $dt->weekNumberInMonth;
            $weekly[$weekNumberInMonth][] = $operation ;


        }



        return $this->calculateCommissionWeekly($weekly);


    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

            $f = $this->argument('argument'); //file path
            $file = fopen($f, "r");

            $pathInfo = pathinfo($f);
            if(!isset($pathInfo['extension'])){
                throw new \Exception('Please Check Your CSV Format');
            }

            $ext = $pathInfo['extension'];
            if($ext != 'csv'){
                throw new \Exception('ONLY CSV FORMAT');
            }

            if ($this->confirm('Want to be changed default fee rate   ?')) {
                $answer = $this->ask('Cash in  natural fee ?',$this->getCommissionFee('cash_in','natural','default'));
                $this->setCommissionFee('cash_in','natural','default',$answer);

                $answer = $this->ask('Cash in  legal fee ?',$this->getCommissionFee('cash_in','legal','default'));
                $this->setCommissionFee('cash_in','legal','default',$answer);

                $answer = $this->ask('Cash out  natural fee ?',$this->getCommissionFee('cash_out','natural','default'));
                $this->setCommissionFee('cash_out','natural','default',$answer);

                $answer = $this->ask('Cash out  legal fee ?',$this->getCommissionFee('cash_out','legal','default'));
                $this->setCommissionFee('cash_out','legal','default',$answer);

            }


            $this->info('Cash in natural user default fee is '. $this->getCommissionFee('cash_in','natural','default'));
            $this->info('Cash in legal user default fee is '. $this->getCommissionFee('cash_in','legal','default'));
            $this->info('Cash out  natural user default fee is '. $this->getCommissionFee('cash_out','natural','default'));
            $this->info('Cash out  legal user default fee is '. $this->getCommissionFee('cash_out','legal','default'));


            $delimiter =  $this->choice(
                'CSV file Delimiter ?',
                ['Comma', 'Semicolon']
            );

            $choiceDelimiters = ['Comma'=>',','Semicolon'=>';'];

            $i = 1;

            $headers = ['USER_ID', 'CASH IN','CASH OUT'];
            $headers_2 = ['TOTAL CASH IN','TOTAL CASH OUT','TOTAL'];


            while ($line = fgetcsv($file, 1000, $choiceDelimiters[$delimiter])) {


                if(count($line) < 6){

                    throw new \Exception('CSV Delimiter is wrong. Please set another Delimiter');
                }


                $map = [
                    'date'=>$line[0],
                    'user_id'=>$line[1],
                    'user_type'=>$line[2],
                    'operation_type'=>$line[3],
                    'amount'=>$line[4],
                    'currency'=>$line[5]
                ];



                if(!in_array($map['user_type'],['natural','legal'])){  //user_type must be natural or legal
                    throw new \Exception('WRONG user type !! line:'.$i);
                }

                if(!in_array($map['operation_type'],['cash_in','cash_out'])){ //operation_type must be cash_in or cash_out
                    throw new \Exception('WRONG operation type !! line:'.$i);
                }


                $output[$map['user_id']][$map['operation_type']][] = $map;

                $i++;

            }



            foreach($output as $user_id=>$operationType){


               $cashIn = $this->getWeeklyOperationTypes($operationType,'cash_in');
                $cashOut = $this->getWeeklyOperationTypes($operationType,'cash_out');

                $summaryCashIn = array_sum($cashIn);
                $summaryCashOut = array_sum($cashOut);

                $data[] = [
                    'user_id'=> $user_id,
                    'cash_in'=>$this->setFee($summaryCashIn), // sum user all cash_in fee
                    'cash_out'=>$this->setFee($summaryCashOut), // sum user all cash_out fee

                ];


                $total['cash_in'][] = $this->setFee($summaryCashIn);
                $total['cash_out'][] = $this->setFee($summaryCashOut);
                $total[] = $this->setFee($summaryCashIn);
                $total[] = $this->setFee($summaryCashOut);


            }


            $data_2[] = [

                'cash_in'=>$this->setFee(array_sum($total['cash_in'])), // sum user all cash_in fee
                'cash_out'=>$this->setFee(array_sum($total['cash_out'])), // sum user all cash_out fee
                'total'=>$this->setFee(array_sum($total)), // sum user all  fee

            ];

            $this->table($headers, $data);
            $this->table($headers_2, $data_2);





        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }


}