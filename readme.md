## PYSR - TASK CALCULATE COMMISSION FEES ON Lumen PHP Framework

HI I'm Marian Ivanov.

 - `composer install` 
 - `cp .env.example .env`
 - `php artisan pysr:input file.csv`

I add more options.

First options if you wanna change fee rate. 

run commands on your terminal after said that:

```
Want to be changed default fee rate   ? (yes/no) [no]:

if answer yes: Starting Q&A for set new  natural and legal users default cash in and cash out fee rate.

Cash in natural fee ? 

Cash in legal fee ?

Cash out natural fee ?

Cash out legal fee ?

```
AND

show your choices


```
Cash in natural user default fee is 0.03

Cash in legal user default fee is 0.03

Cash out  natural user default fee is 0.3

Cash out  legal user default fee is 0.3

```
Last Question

```
 CSV file Delimiter ?
 
  [0] Comma
  
  [1] Semicolon
  

```
 please write  `1`
 
```
+---------+---------+----------+
| USER_ID | CASH IN | CASH OUT |
+---------+---------+----------+
| 1       | 0.06    | 0.60     |
| 2       | 5.00    | 0.90     |
| 3       | 0.00    | 0.00     |
+---------+---------+----------+
```


